Source: p0f
Section: net
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders:
 Pierre Chifflier <pollux@debian.org>,
 Sophie Brun <sophie@freexian.com>,
Standards-Version: 4.5.1
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 libpcap0.8-dev,
Homepage: https://lcamtuf.coredump.cx/p0f3/
Vcs-Git: https://salsa.debian.org/pkg-security-team/p0f.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/p0f

Package: p0f
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Passive OS fingerprinting tool
 p0f performs passive OS detection based on SYN packets. Unlike nmap
 and queso, p0f does recognition without sending any data.
 Additionally, it is able to determine the distance to the remote
 host, and can be used to determine the structure of a foreign or
 local network. When running on the gateway of a network it is able
 to gather huge amounts of data and provide useful statistics. On a
 user-end computer it could be used as powerful IDS add-on. p0f
 supports full tcpdump-style filtering expressions, and has an
 extensible and detailed fingerprinting database.
